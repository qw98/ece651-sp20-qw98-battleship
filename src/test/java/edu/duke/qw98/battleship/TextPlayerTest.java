package edu.duke.qw98.battleship;


    import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import java.io.*;
//test the player

    public class TextPlayerTest {

        private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
            BufferedReader input = new BufferedReader(new StringReader(inputData));
            PrintStream output = new PrintStream(bytes, true);
            Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
            V1ShipFactory shipFactory = new V1ShipFactory();
            return new TextPlayer("A", board, input, output, shipFactory);
        }

        @Test
        void test_read_placement() throws IOException {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            TextPlayer p1= createTextPlayer(10,20,"B2V\nC8H\na4v\n",bytes);

            String prompt = "Please enter a location for a ship:";
            Placement[] expected = new Placement[3];
            expected[0] = new Placement(new Coordinate(1, 2), 'V');
            expected[1] = new Placement(new Coordinate(2, 8), 'H');
            expected[2] = new Placement(new Coordinate(0, 4), 'V');

            for (int i = 0, expectedLength = expected.length; i < expectedLength; i++) {
                Placement placement = expected[i];
                Placement p = p1.readPlacement(prompt);
                assertEquals(p, placement); //did we get the right Placement back
                assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
                bytes.reset(); //clear out bytes for next time around
            }
        }
        @Test
        void test_read_coordinate() throws IOException{
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            TextPlayer player = createTextPlayer(10, 20, "B2\nC8\nA4\n", bytes);

            String prompt = "  Please enter a location for a ship:";
            Coordinate[] expected = new Coordinate[3];
            expected[0] = new Coordinate(1,2);
            expected[1] = new Coordinate(2,8);
            expected[2] = new Coordinate(0,4);

            for (int i = 0; i < expected.length; i++) {
                Coordinate p = player.readCoordinate(prompt);
                assertEquals(p, expected[i]); //did we get the right Placement back
                assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
                bytes.reset(); //clear out bytes for next time around
            }
        }

        @Test
        void test_play_one_round() throws IOException {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            TextPlayer playerA = createTextPlayer(10, 20, "A1\n", bytes);
            TextPlayer playerB = createTextPlayer(10, 20, "A2\n", bytes);

            /* enemy's board */
            V1ShipFactory shipFactory = new V1ShipFactory();
            playerB.getTheBoard().tryAddShip(shipFactory.makeDestroyer(new Placement("B2V")));

            String prompt = "Player A where do you want to place a Destroyer?\n";
            String expectedDis = "  0|1|2|3|4|5|6|7|8|9\n" +
                    "A  | | | | | | | | |  A\n" +
                    "B  | |d| | | | | | |  B\n" +
                    "C  | |d| | | | | | |  C\n" +
                    "D  | |d| | | | | | |  D\n" +
                    "E  | | | | | | | | |  E\n" +
                    "F  | | | | | | | | |  F\n" +
                    "G  | | | | | | | | |  G\n" +
                    "H  | | | | | | | | |  H\n" +
                    "I  | | | | | | | | |  I\n" +
                    "J  | | | | | | | | |  J\n" +
                    "K  | | | | | | | | |  K\n" +
                    "L  | | | | | | | | |  L\n" +
                    "M  | | | | | | | | |  M\n" +
                    "N  | | | | | | | | |  N\n" +
                    "O  | | | | | | | | |  O\n" +
                    "P  | | | | | | | | |  P\n" +
                    "Q  | | | | | | | | |  Q\n" +
                    "R  | | | | | | | | |  R\n" +
                    "S  | | | | | | | | |  S\n" +
                    "T  | | | | | | | | |  T\n" +
                    "  0|1|2|3|4|5|6|7|8|9\n";

            assertEquals(expectedDis,playerB.getView().displayMyOwnBoard());
            assertEquals("You missed!",playerA.playOneTurn(playerB.getTheBoard(),playerB.getView(),playerB.getTextPlayer()));
        }




    }


