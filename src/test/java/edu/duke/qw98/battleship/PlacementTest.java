package edu.duke.qw98.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
    @Test
    public void test_placement() {
        Coordinate c1 = new Coordinate("C1");
        Coordinate c2 = new Coordinate("C1");
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement("C1v");
        assertEquals(p1, p2);
        assertEquals(p1, p3);
    }
    @Test
    public void test_string_equal() {
        Coordinate c1 = new Coordinate("C1");
        Coordinate c2 = new Coordinate("C1");
        Placement p1 = new Placement(c1,'V');
        Placement p2 = new Placement(c2,'v');
        Placement p3 = new Placement("C1H");
        Placement p4 = new Placement("A2V");
        assertEquals(p1.toString(), p2.toString());
        assertEquals(p1.getWhere(), p2.getWhere());
        assertEquals(p1.getOrientation(), p2.getOrientation());
        assertNotEquals(p1.toString(), p3.toString());
        assertNotEquals(p1.toString(), p4.toString());
        assertNotEquals(p1,1);

    }
    @Test
    public void test_hashCode_equal() {
        Placement p1 = new Placement("C1V");
        Placement p2 = new Placement("C1v");
        Placement p3 = new Placement("C1H");
        Placement p4 = new Placement("A2V");
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }
    @Test
    void test_string_constructor_valid_cases() {
        Placement c1 = new Placement("B3V");
        assertEquals(1, c1.where.getRow());
        assertEquals(3, c1.where.getColumn());
        assertEquals('V', c1.getOrientation());
        Placement c2 = new Placement("D5H");
        assertEquals(3, c2.where.getRow());
        assertEquals(5, c2.where.getColumn());
        assertEquals('H', c2.getOrientation());
        Placement c3 = new Placement("A9v");
        assertEquals(0, c3.where.getRow());
        assertEquals(9, c3.where.getColumn());
        assertEquals('V', c3.getOrientation());
        Placement c4 = new Placement("Z0h");
        assertEquals(25, c4.where.getRow());
        assertEquals(0, c4.where.getColumn());
        assertEquals('H', c4.getOrientation());

    }
    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("00f"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1Ad"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0s"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A3f/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("Aw:"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("Ap"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1w2"));
    }
}
