package edu.duke.qw98.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InBoundsRuleCheckerTest {
    @Test
    public void test_check_MyRule(){
        Coordinate c1 = new Coordinate(1,2);
        RectangleShip s1=new RectangleShip("test1",c1, 1,2,'s', '*');
        RectangleShip s2=new RectangleShip("test2",c1, 1,5,'s', '*');
        RectangleShip s3=new RectangleShip("test3",c1, 5,2,'s', '*');
        Coordinate c2 = new Coordinate(-1,2);
        Coordinate c3 = new Coordinate(1,-2);
        RectangleShip s4=new RectangleShip("test4",c2, 2,3,'s', '*');
        RectangleShip s5=new RectangleShip("test5",c3, 2,3,'s', '*');
        InBoundsRuleChecker<Character> i1=new InBoundsRuleChecker<>(null);
        InBoundsRuleChecker<Character> i2=new InBoundsRuleChecker<>(null);
        InBoundsRuleChecker<Character> i3=new InBoundsRuleChecker<>(null);
        InBoundsRuleChecker<Character> i4=new InBoundsRuleChecker<>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard(5, 5,'X');
        assertEquals(null,i1.checkPlacement(s1,b1));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.",i1.checkPlacement(s2,b1));
        assertEquals("That placement is invalid: the ship goes off the right of the board.",i2.checkPlacement(s3,b1));
        assertEquals("That placement is invalid: the ship goes off the top of the board.",i3.checkPlacement(s4,b1));
        assertEquals("That placement is invalid: the ship goes off the left of the board.",i4.checkPlacement(s5,b1));
    }


}
