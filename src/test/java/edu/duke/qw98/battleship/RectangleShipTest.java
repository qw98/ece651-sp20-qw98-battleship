package edu.duke.qw98.battleship;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.HashSet;


public class RectangleShipTest {
    @Test
    public void test_make_coords() {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        HashSet<Coordinate> h1=new HashSet<Coordinate>();
        h1.add(c1);
        h1.add(c2);
        h1.add(c3);
        RectangleShip s1=new RectangleShip("test",c1, 1,3,'s', '*');
        RectangleShip s2=new RectangleShip(c2,'s', '*');
        assertEquals(true,s1.occupiesCoordinates(c1));
        assertEquals(true,s1.occupiesCoordinates(c2));
        assertEquals(true,s1.occupiesCoordinates(c3));
        assertEquals("test",s1.getName());
    }

    @Test
    public void test_record_hit_at() {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        Coordinate c4 = new Coordinate(4,2);
        RectangleShip s1=new RectangleShip("test",c1, 1,3,'s', '*');
        s1.recordHitAt(c2);
        assertEquals(true,s1.wasHitAt(c2));
        assertEquals(false,s1.wasHitAt(c3));
        assertThrows(IllegalArgumentException.class, () -> s1.recordHitAt(c4));
        assertThrows(IllegalArgumentException.class, () -> s1.wasHitAt(c4));
    }

    @Test
    public void test_is_sunk() {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        RectangleShip s1=new RectangleShip("test",c1, 1,3,'s', '*');
        s1.recordHitAt(c1);
        s1.recordHitAt(c2);
        assertEquals(false,s1.isSunk());
        s1.recordHitAt(c3);
        assertEquals(true,s1.isSunk());
    }

    @Test
    public void test_get_display_info_at() {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        RectangleShip s1=new RectangleShip("test",c1, 1,3,'s', '*');
        s1.recordHitAt(c1);
        s1.recordHitAt(c2);
        assertEquals('*',s1.getDisplayInfoAt(c1,true));
        assertEquals('s',s1.getDisplayInfoAt(c3,true));
    }

    @Test
    public void test_get_Coordinates(){
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        RectangleShip s1=new RectangleShip("test",c1, 1,3,'s', '*');
        HashSet<Coordinate>h2=new HashSet<Coordinate>();
        h2.add(c1);
        h2.add(c2);
        h2.add(c3);
        assertEquals(h2,s1.myPieces.keySet());
    }
}
