package edu.duke.qw98.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NoCollisionRuleCheckerTest {
    @Test
    public void test_check_MyRule(){
        BattleShipBoard<Character> b1 = new BattleShipBoard(5, 5,'X');
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,4);
        RectangleShip s1=new RectangleShip("test1",c1, 2,2,'s', '*');
        RectangleShip s2=new RectangleShip("test2",c2, 2,2,'s', '*');
        RectangleShip s3=new RectangleShip("test3",c3, 2,1,'s', '*');
        PlacementRuleChecker<Character> i1=new NoCollisionRuleChecker<Character>(null);
        b1.tryAddShip(s1);
        assertEquals("That placement is invalid: the ship overlaps another ship.",i1.checkPlacement(s2,b1));
        assertEquals(null,i1.checkPlacement(s3,b1));


    }
    @Test
    public void test_check_both_MyRule() {
        BattleShipBoard<Character> b1 = new BattleShipBoard(5, 5,'X');
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 3);
        Coordinate c3 = new Coordinate(4, 3);
        RectangleShip s1 = new RectangleShip("test1", c1, 2, 1, 's', '*');
        RectangleShip s2 = new RectangleShip("test2", c2, 2, 1, 's', '*');
        RectangleShip s3 = new RectangleShip("test3", c3, 1, 1, 's', '*');
        PlacementRuleChecker<Character> i1 = new NoCollisionRuleChecker<Character>(null);
        b1.tryAddShip(s1);
        i1.checkPlacement(s2, b1);
        PlacementRuleChecker<Character> i2 = new InBoundsRuleChecker<>(i1);
        PlacementRuleChecker<Character> i3 = new InBoundsRuleChecker<>(null);
        assertEquals("That placement is invalid: the ship overlaps another ship.", i2.checkPlacement(s2, b1));
        assertEquals("That placement is invalid: the ship overlaps another ship.", i1.checkPlacement(s2, b1));
        assertEquals(null, i3.checkPlacement(s3, b1));
    }
}
