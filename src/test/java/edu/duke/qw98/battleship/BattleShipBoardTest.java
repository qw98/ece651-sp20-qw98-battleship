package edu.duke.qw98.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, 0,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(0, 20,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, -5,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(-8, 20,'X'));
    }

    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard(10, 20,'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
        for (int i = 0; i < b.getHeight(); i++) {
            for (int j = 0; j < b.getWidth(); j++) {
                assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
            }
        }
    }

    @Test
    public void test_what_is_at_board() {
        BattleShipBoard<Character> b1 = new BattleShipBoard(10, 20,'X');
        Character[][] e1 = new Character[20][10];
        checkWhatIsAtBoard(b1, e1);

        BattleShipBoard<Character> b2 = new BattleShipBoard(10, 20,'X');
        Character[][] e2 = new Character[20][10];
        addShipToBoard(b2, new String[]{"A1", "B5"});
        addShipToExpect(e2,new String[]{"A1", "B5"});
        checkWhatIsAtBoard(b2, e2);
    }

    private void addShipToBoard(BattleShipBoard<Character> b, String[] s) {
        for (int i = 0; i < s.length; i++) {
            RectangleShip<Character> bs = new RectangleShip<Character>(new Coordinate(s[i]), 's', '*');
            assertEquals(null,b.tryAddShip(bs));
        }
    }

    private void addShipToExpect(Character[][]expected, String[] s) {
        for (int i = 0; i < s.length; i++) {
            Coordinate c=new Coordinate(s[i]);
            expected[c.getRow()][c.getColumn()]='s';
        }
    }

    @Test
    public void test_try_Add_Ship() {
        BattleShipBoard<Character> b1 = new BattleShipBoard(10, 20,'X');
        Coordinate c1 = new Coordinate(1,2);
        RectangleShip s1=new RectangleShip("test1",c1, 1,2,'s', '*');
        assertEquals(null,b1.tryAddShip(s1));
        RectangleShip s2=new RectangleShip("test2",c1, 2,1,'s', '*');
        assertEquals("That placement is invalid: the ship overlaps another ship.",b1.tryAddShip(s2));
        Coordinate c2 = new Coordinate(9,9);
        RectangleShip s3=new RectangleShip("test3",c2, 2,2,'s', '*');
        assertEquals("That placement is invalid: the ship goes off the right of the board.",b1.tryAddShip(s3));
    }


@Test
void test_all_ship_sunk(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory v1ShipFactory = new V1ShipFactory();
    Placement v1 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);

    b1.tryAddShip(dst);
    Coordinate c1 = new Coordinate(1,2);
    Coordinate c2 = new Coordinate(2,2);
    Coordinate c3 = new Coordinate(3,2);
    Coordinate c4 = new Coordinate(4,2);

    b1.fireAt(c1);
    assertFalse(b1.allShipSunk());
    b1.fireAt(c4);
    assertFalse(b1.allShipSunk());
    b1.fireAt(c2);
    assertFalse(b1.allShipSunk());
    b1.fireAt(c3);
    assertTrue(b1.allShipSunk());
}

}


