package edu.duke.qw98.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//the test of the new version ship factory

public class V2ShipFactoryTest {

    @Test
    void test_invalid_placement(){
        V2ShipFactory v2ShipFactory = new V2ShipFactory();

        //Destroyer
        Placement test1 = new Placement(new Coordinate(1, 2), 'A');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeDestroyer(test1));

        //Battleship
        Placement test2 = new Placement(new Coordinate(3, 4), 'T');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeBattleship(test2));

        //Battleship
        Placement test3 = new Placement(new Coordinate(7, 8), 'V');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeBattleship(test3));

        //Destroyer
        Placement test4 = new Placement(new Coordinate(5, 6), 'Q');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeCarrier(test4));
    }


    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
        assertEquals(expectedName,testShip.getName());
        for(Coordinate c : expectedLocs){
            assertTrue(testShip.occupiesCoordinates(c));
            assertEquals(expectedLetter,testShip.getDisplayInfoAt(c,true));
        }
    }

    @Test
    void test_make_ships(){
        V2ShipFactory v2ShipFactory = new V2ShipFactory();

        // Battleship
        Placement t1 = new Placement(new Coordinate(2, 2), 'U');
        Ship<Character> battleship = v2ShipFactory.makeBattleship(t1);
        checkShip(battleship, "Battleship", 'b', new Coordinate(2, 3), new Coordinate(3, 2),
                new Coordinate(3, 3), new Coordinate(3, 4));
        // Carrier
        Placement t2 = new Placement(new Coordinate(1, 2), 'L');
        Ship<Character> carrier = v2ShipFactory.makeCarrier(t2);
        checkShip(carrier, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
                new Coordinate(2, 4), new Coordinate(2, 5), new Coordinate(2, 6));

        // Destroyer
        Placement t3 = new Placement(new Coordinate(2, 1), 'H');
        Ship<Character> dst1 = v2ShipFactory.makeDestroyer(t3);
        checkShip(dst1, "Destroyer", 'd', new Coordinate(2, 1), new Coordinate(2, 2), new Coordinate(2, 3));

        // Submarine
        Placement t4 = new Placement(new Coordinate(3, 3), 'H');
        Ship<Character> sub = v2ShipFactory.makeSubmarine(t4);
        checkShip(sub, "Submarine", 's', new Coordinate(3, 3), new Coordinate(3, 4));


    }




}

