package edu.duke.qw98.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs){
        assertEquals(expectedName,testShip.getName());
        for(Coordinate c:expectedLocs){
            assertEquals(expectedLetter,testShip.getDisplayInfoAt(c,true));
            assertEquals(true,testShip.occupiesCoordinates(c));
        }
    }



    /**
     *
     * the test of 4 different ships
     */
    @Test
    public void test_make_Destroyer(){
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        V1ShipFactory f=new V1ShipFactory();
        Ship<Character> s1 = f.makeDestroyer(v1_2);
        Ship<Character> s2 = f.makeDestroyer(h1_2);
        checkShip(s2, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
        checkShip(s1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));

    }
    @Test
    public void test_make_makeCarrier(){
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement h4_5 = new Placement(new Coordinate(4, 5), 'H');
        V1ShipFactory f=new V1ShipFactory();
        Ship<Character> s1 = f.makeCarrier(v1_2);
        checkShip(s1, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2),new Coordinate(3, 2),new Coordinate(4, 2),new Coordinate(5, 2),new Coordinate(6, 2));
        Ship<Character> s2 = f.makeCarrier(h4_5);
        checkShip(s2, "Carrier", 'c', new Coordinate(4, 5), new Coordinate(4, 6),new Coordinate(4, 7),new Coordinate(4, 8),new Coordinate(4, 9),new Coordinate(4, 10));
    }
    @Test
    public void test_make_Submarine(){
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement h2_3 = new Placement(new Coordinate(2, 3), 'H');
        V1ShipFactory f=new V1ShipFactory();
        Ship<Character> s1 = f.makeSubmarine(v1_2);
        checkShip(s1, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));
        Ship<Character> s2 = f.makeSubmarine(h2_3);
        checkShip(s2, "Submarine", 's', new Coordinate(2, 3), new Coordinate(2, 4));
    }

    @Test
    public void test_make_Battleship(){
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement h2_3 = new Placement(new Coordinate(2, 3), 'H');
        V1ShipFactory f=new V1ShipFactory();
        Ship<Character> s1 = f.makeBattleship(v1_2);
        checkShip(s1, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2),new Coordinate(3, 2),new Coordinate(4, 2));
        Ship<Character> s2 = f.makeBattleship(h2_3);
        checkShip(s2, "Battleship", 'b', new Coordinate(2, 3), new Coordinate(2, 4),new Coordinate(2, 5),new Coordinate(2, 6));
    }


}