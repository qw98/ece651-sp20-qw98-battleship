package edu.duke.qw98.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T>{

    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    public final Coordinate movePoint;
    public char orientation;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo,
                     Coordinate movePoint, char orientation){
        this.myDisplayInfo            = myDisplayInfo;
        this.enemyDisplayInfo         = enemyDisplayInfo;
        myPieces                      = new HashMap<>();
        for (Coordinate coordinate : where) {
            myPieces.put(coordinate, false);
        }

        this.movePoint    = movePoint;
        this.orientation = orientation;
    }

    public BasicShip(HashMap<Coordinate, Boolean> pieces, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo,
                     Coordinate movePoint, char orientation){
        this.myDisplayInfo    = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        myPieces              = new HashMap<>();
        this.movePoint         = movePoint;
        this.orientation      = orientation;

        for(Coordinate coordinate : pieces.keySet()){
            myPieces.put(coordinate, pieces.get(coordinate));
        }
    }

    protected BasicShip(ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo,
                        Coordinate movePoint, char orientation){
        this.myDisplayInfo    = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        myPieces              = new HashMap<>();
        this.movePoint         = movePoint;
        this.orientation      = orientation;
    }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        if(myPieces.get(where)==null) return false;
        return true;
    }

    @Override
    public boolean isSunk() {
        for(boolean sunk:myPieces.values()){
            if(sunk==false){
                return false;
            }
        }
        return true;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.replace(where,false,true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    @Override
    public T getDisplayInfoAt(Coordinate where,boolean myShip){
        if (myShip){
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        else {
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
        }
    }

    protected void checkCoordinateInThisShip(Coordinate c){
        if(myPieces.get(c)==null){
            throw new IllegalArgumentException("is not part of the ship");
        }
    }
    public Iterable<Coordinate> getCoordinates(){
        return myPieces.keySet();
    }


}
