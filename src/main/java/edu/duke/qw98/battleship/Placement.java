package edu.duke.qw98.battleship;
/**
 * This is the placement of the ship on the board
 */
public class Placement {
    final Coordinate where;
    final char orientation;

    public Placement(Coordinate where, char orientation) {
        this.where = where;
        this.orientation = Character.toUpperCase(orientation);
    }

    public Coordinate getWhere() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement c = (Placement) o;
            return where.equals(c.where) && orientation == c.orientation;
        }
        return false;
    }
    @Override
    public String toString() {
        return where+", " + orientation;
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public Placement(String desc) {
        String newDesc = desc.toUpperCase();
        int len = newDesc.length();
        if(len != 3){
            throw new IllegalArgumentException("Placement descriptions must be length 3, but is " + desc);
        }
        if(newDesc.charAt(2)!= 'V' && newDesc.charAt(2)!= 'H' && newDesc.charAt(2)!= 'U' && newDesc.charAt(2)!= 'R' && newDesc.charAt(2)!= 'D' && newDesc.charAt(2)!= 'L'){
            throw new IllegalArgumentException("Placement descriptions must be length 3, but is " + desc);
        }
        where = new Coordinate(newDesc.substring(0,2));
        orientation = newDesc.charAt(2);
    }
}
