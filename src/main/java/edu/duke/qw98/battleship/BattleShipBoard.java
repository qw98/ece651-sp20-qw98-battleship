package edu.duke.qw98.battleship;


import java.util.ArrayList;
import java.util.HashSet;

/**
 * This create the Battle ship board
 *
 */
public class BattleShipBoard<T> implements Board<T>{

    final ArrayList<Ship<T>> myShips;
    final int height;
    final int width;
    HashSet<Coordinate> enemyMisses;
    final T missInfo;

    public BattleShipBoard(int width, int height, PlacementRuleChecker<T> placementChecker,T missInfo) {
        this.placementChecker = placementChecker;

        if (width <= 0||height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width and height are positive!");
        }
        this.width=width;
        this.height=height;
        this.missInfo=missInfo;
        myShips = new ArrayList<>();
        enemyMisses=new HashSet<>();
    }
    public int getWidth() {
        return width;
    }
    public int getHeight(){
        return height;
    }

    /**
     * This try  to add ship to the board
     *
     * @return the return true if successfully add ship
     */
    public String tryAddShip(Ship<T> toAdd){ //string
        String placementProblem = placementChecker.checkPlacement(toAdd, this);
        if (placementProblem== null) {
            myShips.add(toAdd);
            return null;
        }
        return placementProblem;
    }
    /**
     * This take the coordinate and show whether there is a ship
     *
     * @return the return value of getDisplayInfo
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    protected T whatIsAt(Coordinate where, boolean isSelf){
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s.getDisplayInfoAt(where,isSelf);
            }
        }
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }
        return null;
    }

    private final PlacementRuleChecker<T> placementChecker;
    public BattleShipBoard(int w, int h,T missInfo) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)),missInfo);
    }
    /**
     * @Description: fire at the ship
     * @Param:       Coordinate c
     * @return:       Ship<T>
     **/
    public Ship<T> fireAt(Coordinate c){
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c)){
                s.recordHitAt(c);
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }
    /**
     * @Description: judge if all ships are sunk
     * @Param:
     * @return:      boolean
     **/
    public boolean allShipSunk(){
        for(Ship<T> s : myShips){
            if(!s.isSunk()){
                return false;
            }
        }
        return true;
    }

    /**
     * This function is used to get the information of the point.
     * get which ship is at the point regardless of its situation(hit or not)
     */
    private T SonarScanShipInfo(Coordinate coordinate) {
        for(Ship<T> theShip : myShips){
            if(theShip.occupiesCoordinates(coordinate)){
                BasicShip<T> basicShip = (BasicShip<T>) theShip;
                return basicShip.myDisplayInfo.getInfo(coordinate, false);
            }
        }

        return null;
    }
    /**
     * scan the points around the Target(the diamond)
     * and and increase the num of pieces of different ship
     */
    @Override
    public String ScanAndFind(Coordinate Target) {
        int row      = Target.getRow();
        int col      = Target.getColumn();
        int sRow = row - 3;
        int sCol = col - 3;
        //these nums is used to record the num of pieces of corresponding ship
        int SubNums  = 0;
        int DestroyNums  = 0;
        int BattleNums = 0;
        int CarriNums    = 0;
        //add the points in the pattern to the HashSet
        HashSet<Coordinate> starPoints = new HashSet<>();
        for(int i = sRow; i <= sRow + 3; i++){
            int begin = sCol + 3 - (i - sRow);
            int nums = 2 * (i - sRow) + 1;
            while(nums > 0){
                if(i >= 0 && i < height &&  begin >= 0 && begin < width)
                    starPoints.add(new Coordinate(i, begin));
                nums--;
                begin++;
            }
        }
        for(int i = row + 3; i >= row + 1; i--){
            int begin = i + sCol  - row;
            int starNum = 1 + 2 * (3 - (i - row)) ;
            while(starNum > 0){
                if(i >= 0 && i < height &&  begin >= 0 && begin < width)
                    starPoints.add(new Coordinate(i, begin));
                begin++;
                starNum--;
            }
        }
        for(Coordinate c : starPoints){

            char ch;
            if(SonarScanShipInfo(c)==null){
                ch = '*';
            }
            else{
                ch = (char) SonarScanShipInfo(c);
            }
            if(ch == 's'){
                SubNums++;
            }
            else if(ch == 'd'){
                DestroyNums++;
            }
            else if(ch == 'b'){
                BattleNums++;
            }
            else if(ch == 'c'){
                CarriNums++;
            }
            else if(ch == '*'){
                continue;
            }
        }
        //generate the output
        StringBuilder report = new StringBuilder();
        report.append("-------------------------------------------------------------------------\n")
                .append("Submarines occupy ").append(SubNums).append(" squares\n")
                .append("Destroyers occupy ").append(DestroyNums).append(" squares\n")
                .append("Battleships occupy ").append(BattleNums).append(" squares\n")
                .append("Carriers occupy ").append(CarriNums).append(" squares\n")
                .append("-------------------------------------------------------------------------\n");

        return report.toString();
    }

}
