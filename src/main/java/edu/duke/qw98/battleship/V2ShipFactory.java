package edu.duke.qw98.battleship;


import java.util.HashMap;

/**
 *  This is the version2 ship factory ,add new Battle ship and carrier. and some methods for the movement operations.
 */


public class V2ShipFactory implements AbstractShipFactory<Character>{


    private final V1ShipFactory v1ShipFactory = new V1ShipFactory();


    @Override
    public  Ship<Character> makeSubmarine(Placement where) throws IllegalArgumentException{
        return v1ShipFactory.makeSubmarine(where);
    }
    @Override
    public  Ship<Character> makeDestroyer(Placement where)throws IllegalArgumentException {
        return v1ShipFactory.makeDestroyer(where);
    }
    @Override
    public  Ship<Character> makeBattleship(Placement where) throws IllegalArgumentException{
        ShipDisplayInfo<Character> self    = new SimpleShipDisplayInfo<Character>('b', '*');
        ShipDisplayInfo<Character> enemy = new SimpleShipDisplayInfo<Character>(null, 'b');
        return new V2BattleShip<Character>("Battleship", where.getOrientation(),where.getWhere(), self, enemy);
    }
    @Override
    public  Ship<Character> makeCarrier(Placement where) throws IllegalArgumentException{
        ShipDisplayInfo<Character> self    = new SimpleShipDisplayInfo<Character>('c', '*');
        ShipDisplayInfo<Character> enemy = new SimpleShipDisplayInfo<Character>(null, 'c');

        return new V2Carriers<Character>("Carrier", where.getOrientation(),where.getWhere(), self, enemy);
    }

    /**
     * the methods are used for movement of different ships
     */
    public Ship<Character> makeNewShip(Placement newLocation, Ship<Character> OldOne)throws IllegalArgumentException{
        Ship<Character> ship = null;
        if(OldOne.getName().equals("Carrier"))
            ship = carrierMovement(newLocation, OldOne);
        else if(OldOne.getName().equals("Submarine"))
            ship = submarineMovement(newLocation, OldOne);
        else if(OldOne.getName().equals("Battleship"))
            ship =  battleShipMovement(newLocation, OldOne);
        else if(OldOne.getName().equals("Destroyer"))
            ship = destroyerMovement(newLocation, OldOne);

        return ship;
    }

    public Ship<Character> carrierMovement(Placement newLocation, Ship<Character> oldOne)throws IllegalArgumentException{
        V2Carriers<Character> ship = (V2Carriers<Character>) oldOne;

        /* step 1, make a completely newShip */
        V2Carriers<Character> newShip = (V2Carriers<Character>) makeCarrier(newLocation);

        /* step2, update the hit information */
        for(Integer number : ship.piecesToMove.keySet()){
            if(ship.myPieces.get(ship.piecesToMove.get(number)) == true){
                newShip.myPieces.put(newShip.piecesToMove.get(number), true);
            }
        }

        return newShip;
    }



    public Ship<Character> battleShipMovement(Placement newLocation, Ship<Character> oldOne)throws IllegalArgumentException{
        V2BattleShip<Character> ship = (V2BattleShip<Character>) oldOne;
        V2BattleShip<Character> newShip = (V2BattleShip<Character>) makeBattleship(newLocation);

        for(Integer number : ship.piecesToMove.keySet()){
            if(ship.myPieces.get(ship.piecesToMove.get(number)) == true){
                newShip.myPieces.put(newShip.piecesToMove.get(number), true);
            }
        }

        return newShip;
    }




    public Ship<Character> submarineMovement(Placement newLocation, Ship<Character> oldOne)throws IllegalArgumentException{
        BasicShip<Character> ship = (BasicShip<Character>) oldOne;
        HashMap<Coordinate, Boolean> pieces = new HashMap<>();
        if(ship.orientation != newLocation.getOrientation()){
            Coordinate secondOld = null;
            Coordinate secondNew = null;
             if(ship.orientation != 'V' &&ship.orientation != 'H'||(newLocation.getOrientation() != 'H'&&newLocation.getOrientation() != 'V')){
                 throw new IllegalArgumentException("The orientation of submarine is wrong!");
                 }
            if(ship.orientation == 'V' && newLocation.getOrientation() == 'H'){
                secondOld = new Coordinate(ship.movePoint.getRow() + 1, ship.movePoint.getColumn());
                secondNew = new Coordinate(newLocation.getWhere().getRow(), newLocation.getWhere().getColumn() + 1);
            }
            else{
                secondOld = new Coordinate(ship.movePoint.getRow(), ship.movePoint.getColumn() + 1) ;
                secondNew = new Coordinate(newLocation.getWhere().getRow() + 1, newLocation.getWhere().getColumn());
            }
            pieces.put(newLocation.getWhere(), ship.myPieces.get(ship.movePoint));
            pieces.put(secondNew, ship.myPieces.get(secondOld));
            return new RectangleShip<Character>("Submarine", pieces, 's', '*', newLocation.getWhere(), newLocation.getOrientation());
        }
        else{
            int rowsToMove = newLocation.getWhere().getRow() - ship.movePoint.getRow();
            int colsToMove = newLocation.getWhere().getColumn() - ship.movePoint.getColumn();

            for(Coordinate coordinate : ship.myPieces.keySet()){
                int newRow = coordinate.getRow() + rowsToMove;
                int newCol = coordinate.getColumn() + colsToMove;

                pieces.put(new Coordinate(newRow, newCol), ship.myPieces.get(coordinate));
            }
            return new RectangleShip<Character>("Submarine", pieces, 's', '*', newLocation.getWhere(), newLocation.getOrientation());
        }
    }

    public Ship<Character> destroyerMovement(Placement newLocation, Ship<Character> oldOne)throws IllegalArgumentException{
        BasicShip<Character> ship = (BasicShip<Character>) oldOne;
        HashMap<Coordinate, Boolean> pieces = new HashMap<>();
        if(ship.orientation != newLocation.getOrientation()){
            Coordinate newOtherPoints = null;
            Coordinate oldOtherPoints = null;
            if(ship.orientation != 'V' &&ship.orientation != 'H'||(newLocation.getOrientation() != 'H'&&newLocation.getOrientation() != 'V')){
                throw new IllegalArgumentException("The orientation of Destroyer is wrong!");
            }
            pieces.put(newLocation.getWhere(), ship.myPieces.get(ship.movePoint));
            for(int i = 1; i <= 2; i++){
                if(ship.orientation == 'V' && newLocation.getOrientation() == 'H'){
                    oldOtherPoints = new Coordinate(ship.movePoint.getRow() + i, ship.movePoint.getColumn());
                    newOtherPoints = new Coordinate(newLocation.getWhere().getRow(), newLocation.getWhere().getColumn() + i);
                }else if(ship.orientation == 'H' && newLocation.getOrientation() == 'V'){
                    oldOtherPoints = new Coordinate(ship.movePoint.getRow(), ship.movePoint.getColumn() + i);
                    newOtherPoints = new Coordinate(newLocation.getWhere().getRow() + i, newLocation.getWhere().getColumn());
                }else{
                    throw new IllegalArgumentException("[Error] In Class V2ShipFactory, Submarine Orientation is illegal");
                }
                pieces.put(newOtherPoints, ship.myPieces.get(oldOtherPoints));
            }
            return new RectangleShip<Character>("Destroyer", pieces, 'd', '*', newLocation.getWhere(), newLocation.getOrientation());

        }
        else{
            int rowsToMove = newLocation.getWhere().getRow() - ship.movePoint.getRow();
            int colsToMove = newLocation.getWhere().getColumn() - ship.movePoint.getColumn();

            for(Coordinate coordinate : ship.myPieces.keySet()){
                int newRow = coordinate.getRow() + rowsToMove;
                int newCol = coordinate.getColumn() + colsToMove;

                pieces.put(new Coordinate(newRow, newCol), ship.myPieces.get(coordinate));
            }
            return new RectangleShip<Character>("Destroyer", pieces, 'd', '*', newLocation.getWhere(), newLocation.getOrientation());
        }
    }





}
