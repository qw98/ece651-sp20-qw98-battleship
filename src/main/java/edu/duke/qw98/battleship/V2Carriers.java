package edu.duke.qw98.battleship;

/**
 * @description: Class of Carrier of version 2
 **/

public class V2Carriers<T>  extends V2Ship<T>  {
    public V2Carriers(String name, char orientation, Coordinate refPoint, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(name, orientation, refPoint, myDisplayInfo, enemyDisplayInfo);
    }

    public void addMovePiece(int x, int y, int i, int j,int order){
        this.myPieces.put(new Coordinate(x+i,y+j),false);
        this.piecesToMove.put(order, new Coordinate(x+i, y+j));
    }

    @Override
    public void makeCoords(){
        int x = movePoint.getRow();
        int y = movePoint.getColumn();

        if(this.orientation == 'U'){
            addMovePiece(x, y, 0, 0,1);
            addMovePiece(x, y, 1, 0,2);
            addMovePiece(x, y, 2, 0,3);
            addMovePiece(x, y, 2, 1,4);
            addMovePiece(x, y, 3, 1,5);
            addMovePiece(x, y, 4, 1,6);

        }else if(this.orientation == 'R'){
            addMovePiece(x, y, 0, 2,4);
            addMovePiece(x, y, 0, 3,5);
            addMovePiece(x, y, 0, 4,6);
            addMovePiece(x, y, 1, 0,1);
            addMovePiece(x, y, 1, 1,2);
            addMovePiece(x, y, 1, 2,3);

        }else if(this.orientation == 'D'){
            addMovePiece(x, y, 0, 1,6);
            addMovePiece(x, y, 1, 1,5);
            addMovePiece(x, y, 2, 1,4);
            addMovePiece(x, y, 2, 0,3);
            addMovePiece(x, y, 3, 0,2);
            addMovePiece(x, y, 4, 0,1);

        }else if(this.orientation == 'L'){
            addMovePiece(x, y, 0, 0,6);
            addMovePiece(x, y, 0, 1,5);
            addMovePiece(x, y, 0, 2,4);
            addMovePiece(x, y, 1, 2,3);
            addMovePiece(x, y, 1, 3,2);
            addMovePiece(x, y, 1, 4,1);

        }else{
            throw new IllegalArgumentException("Wrong Input!");
        }
    }


    @Override
    public String getName() {
        return name;
    }
}


