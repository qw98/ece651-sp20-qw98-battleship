package edu.duke.qw98.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    /**
     * This is the function to check my rule if the ship is in the board
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        // TODO Auto-generated method stub
        for(Coordinate c:theShip.getCoordinates()){
            if(c.getRow()<0){
                return "That placement is invalid: the ship goes off the top of the board.";
            }
            if(c.getRow()>=theBoard.getHeight()){
                return "That placement is invalid: the ship goes off the bottom of the board.";
            }
            if(c.getColumn()<0){
                return "That placement is invalid: the ship goes off the left of the board.";
            }
            if(c.getColumn()>=theBoard.getWidth()){
                return "That placement is invalid: the ship goes off the right of the board.";

            }
        }
        return null;
    }
}