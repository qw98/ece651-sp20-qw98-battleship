package edu.duke.qw98.battleship;
/**
 * This is the function to check my rule
 */
public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        if (checkMyRule(theShip, theBoard)!=null) {
            return checkMyRule(theShip, theBoard);
        }
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        return null;
    }
}
