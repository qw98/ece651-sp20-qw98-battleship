package edu.duke.qw98.battleship;

import java.util.Locale;
/**
 * This is the coordinate on the board
 */
public class Coordinate {

    private final int row;
    private final int column;

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public Coordinate(String desc){
        desc = desc.toUpperCase();
        int len = desc.length();
        if(len != 2){
            throw new IllegalArgumentException("wrong string size!");
        }
        if(desc.charAt(0)>'Z'||desc.charAt(0)<'A'||desc.charAt(1)<'0'||desc.charAt(1)>'9'){
            throw new IllegalArgumentException("wrong string content!");
        }
        this.row = desc.charAt(0)-'A';
        this.column = desc.charAt(1)-'0';
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public String toString() {

        return "("+row+", " + column+")";
    }
    @Override
    public int hashCode() {

        return toString().hashCode();
    }
}
