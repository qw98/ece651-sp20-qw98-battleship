package edu.duke.qw98.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    private final String name;

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> ans = new HashSet<Coordinate>();
        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                ans.add(new Coordinate(upperLeft.getRow() + row, upperLeft.getColumn() + col));
            }
        }
        return ans;
    }
    public RectangleShip(String name, HashMap<Coordinate, Boolean> pieces, T myData, T onHit, Coordinate refPoint, char orientation){
        super(pieces, new SimpleShipDisplayInfo<T>(myData, onHit), new SimpleShipDisplayInfo<T>(null, myData), refPoint, orientation);
        this.name = name;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo, upperLeft, width > height ? 'H' : 'V');
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    public String getName() {
        return name;
    }
}
