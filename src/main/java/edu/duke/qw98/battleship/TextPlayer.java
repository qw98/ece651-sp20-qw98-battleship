package edu.duke.qw98.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

import static edu.duke.qw98.battleship.Robot.randomCoordinateOfBoard;

public class TextPlayer {

    final BattleShipBoard<Character>  theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    public boolean isRobot;
    //numbers to move or scan in sonar
    public int numsToMovement = 3;
    public int numsToScan = 3;


    public static Coordinate getLocation(BufferedReader inputReader, PrintStream out, String prompt) throws IOException {
        Coordinate point = null;
        while(true) {
            out.println(prompt);
            String strCoordinate = inputReader.readLine();

            try {
                point = new Coordinate(strCoordinate);
                break;
            }catch(IllegalArgumentException e){
                System.out.println("Illegal argument for Coordinate, input again");
            }
        }

        return point;
    }



    public Board<Character> getTheBoard() {
        return theBoard;
    }

    public BoardTextView getView(){
        return view;
    }

    public PrintStream getOut(){
        return out;
    }

    public String getTextPlayer(){
        return name;
    }

    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
                      AbstractShipFactory<Character> shipFactory) {
        this.name = name;
        this.theBoard = (BattleShipBoard<Character>) theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        this.isRobot = false;
        setupShipCreationMap();
        setupShipCreationList();
    }

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("Unexpected EOF on standard input trying to read placement");
        }
        return new Placement(s);
    }
    /**
     * @Description: do a placement on the board
     * @Param:       String shipName, Function<Placement, Ship<Character>> createFn
     * @return:      void
     **/
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        String problem = null;
        do {
            try {
                char[] chars1 = {'V','H'};
                char[] chars2 = {'U','D','L','R'};
                Placement target;
                if(shipName.equals("Battleship")||shipName.equals("Carrier")){
                    String coordinate = randomCoordinateOfBoard();
                    int choice = (int) ((Math.random() * 4) % 4);
                    char ch =  chars2[choice];
                    StringBuilder place  = new StringBuilder();
                    place.append(coordinate).append(ch);
                    target = new Placement(place.toString());
                }
                else{
                    String coordinate = randomCoordinateOfBoard();
                    int choice = (int) ((Math.random() * 2) % 2);
                    char ch =  chars1[choice];
                    StringBuilder place  = new StringBuilder();
                    place.append(coordinate).append(ch);
                    target = new Placement(place.toString());
                }

                Placement p = isRobot? target:readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                problem = theBoard.tryAddShip(s);
            }
            catch (IllegalArgumentException iae) {
                problem = "it does not have the correct format";
            }
            if (problem != null) {
                String mesg = "That placement is invalid: " + problem + ".";
                out.println(mesg);
                out.print(view.displayMyOwnBoard());
            }
        } while (problem != null);
        out.print(view.displayMyOwnBoard());
    }
    /**
     * @Description: the placement phase
     * @Param:
     * @return:      void
     **/
    public void doPlacementPhase() throws IOException {
        out.print(view.displayMyOwnBoard());
        String instructions = "Player " + name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n";

        out.print(instructions);
        for (String shipName : shipsToPlace) {
            Function<Placement, Ship<Character>> f = shipCreationFns.get(shipName);
            doOnePlacement(shipName, f);
        }

    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Coordinate(s);
    }


    public String playOneTurn(Board<Character> enemyBoard,BoardTextView enemyView ,String enemyName)throws IOException, IllegalArgumentException{
        try{
            out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your Ocean","Player "+ enemyName + "'s Ocean"));
            Coordinate attack = readCoordinate("Player " + name + " where do you want to attack?");
            enemyBoard.fireAt(attack);
            Character hitRes = enemyBoard.whatIsAtForEnemy(attack);
            if      (hitRes == 's')       return "You hit a submarine!";
            else if (hitRes == 'd')       return "You hit a Destroyer!";
            else if (hitRes == 'b')       return "You hit a Battleship!";
            else if (hitRes == 'c')       return "You hit a Carrier!";
            else                          return "You missed!";

        }
        catch (IllegalArgumentException illegalArgumentException){
            return null;
        }

    }

    /**
     * @Description: decide if we lose
     * @Param:
     * @return:      boolean
     **/
    public boolean lose(){
        return theBoard.allShipSunk();
    }

    protected boolean inBoard(int x, int y){
        return x >= 0 && x < theBoard.getHeight() &&  y >= 0 && y < theBoard.getHeight();
    }

    /**
     * This function is used to locate the ship with given coordinate
     */
    protected Ship<Character> shipLocate(Coordinate movePoint) {
        for(Ship<Character> ship : theBoard.myShips){
            BasicShip<Character> basicShip = (BasicShip<Character>) ship;

            if(basicShip.myPieces.containsKey(movePoint))
                return basicShip;
        }

        return null;
    }


    /**
     * This function is used to move a ship from old square to a new one and with new orientation
     */
    public void moveOneShipToNewSquare() throws IOException {

        Ship<Character> shipToMove = null;
        String info = null;
        while(true){
            try{
                Coordinate point = isRobot ? new Coordinate(randomCoordinateOfBoard()):getLocation(inputReader,
                        out,"Input a coordinate of a ship that you want to move!");

                if(shipToMove == null)
                    shipToMove = shipLocate(point);

                if(shipToMove == null){
                    throw new IllegalArgumentException("Invalid Input of Coordinate");
                }
                Placement newPlacement = readPlacement("Choose a location to place your ship, just like what you did in the placement phase :)");


                theBoard.myShips.remove(shipToMove);

                V2ShipFactory v2 = (V2ShipFactory) shipFactory;
                Ship<Character> newShip = v2.makeNewShip(newPlacement, shipToMove);

                info = theBoard.tryAddShip(newShip);
                if(info == null) {
                    break;
                }else{
                    out.println(info);
                    throw new IllegalArgumentException("Wrong Placement for new Ship");
                }
            }catch (Exception e){
                out.println("Illegal Argument, check your placement and coordinate input");
            }
        }

        BoardTextView view = new BoardTextView(theBoard);
        out.println(view.displayMyOwnBoard());
    }

    /**
     * This function is used to do sonar scan
     *
     * @throws IOException
     */
    public void sonarScan(Board<Character> board) throws IOException {

        while(true){
            Coordinate thePoint = isRobot? new Coordinate(randomCoordinateOfBoard()):getLocation(inputReader,
                    out,"Choose a target coordinate for sonar scan!");

            if(!inBoard(thePoint.getRow(), thePoint.getColumn()))
                continue;

            String s = board.ScanAndFind(thePoint);
            out.println(s);
            break;
        }
    }



}
