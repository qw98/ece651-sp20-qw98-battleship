package edu.duke.qw98.battleship;

public class V1ShipFactory implements AbstractShipFactory{

    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        RectangleShip<Character> s;
        Character orientation = where.getOrientation();
        if(orientation != 'H' && orientation != 'V'){
            throw new IllegalArgumentException("Orientation is not valid!");
        }

        if(where.getOrientation() == 'V'){

            s = new RectangleShip<>(name, where.getWhere(), w, h, letter, '*');

        }
        else{

            s = new RectangleShip<>(name, where.getWhere(), h, w, letter, '*');

        }

        return s;
    }
    @Override
    public Ship makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
    @Override
    public Ship makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }


}
