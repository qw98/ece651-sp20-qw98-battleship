package edu.duke.qw98.battleship;

/**
 * @description: Class of Battleship of version 2
 **/

public class V2BattleShip<T> extends V2Ship<T>  {

    public V2BattleShip(String name, char orientation, Coordinate refPoint, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(name, orientation, refPoint, myDisplayInfo, enemyDisplayInfo);
    }

    public void addMovePiece(int x, int y, int i, int j,int order){
        this.myPieces.put(new Coordinate(x+i,y+j),false);
        this.piecesToMove.put(order, new Coordinate(x+i, y+j));
    }
//this method is used to generate different coordinates of the battle ship
    @Override
    public void makeCoords(){
        int x = movePoint.getRow();
        int y = movePoint.getColumn();

        if(this.orientation == 'U'){
            addMovePiece(x, y,0 ,1,1);
            addMovePiece(x, y,1 ,0,2);
            addMovePiece(x, y,1 ,1,3);
            addMovePiece(x, y,1 ,2,4);
        }else if(this.orientation == 'R'){
            addMovePiece(x, y,0 ,0,2);
            addMovePiece(x, y,1 ,0,3);
            addMovePiece(x, y,2 ,0,4);
            addMovePiece(x, y,1 ,1,1);


        }else if(this.orientation == 'D'){
            addMovePiece(x, y,0 ,0,4);
            addMovePiece(x, y,0 ,1,3);
            addMovePiece(x, y,0 ,2,2);
            addMovePiece(x, y,1 ,1,1);


        }else if(this.orientation == 'L'){
            addMovePiece(x, y,0 ,1,4);
            addMovePiece(x, y,1 ,0,1);
            addMovePiece(x, y,1 ,1,3);
            addMovePiece(x, y,2 ,1,2);

        }else{
            throw new IllegalArgumentException("Wrong Input!");
        }
    }

    @Override
    public String getName() {
        return name;
    }
}


