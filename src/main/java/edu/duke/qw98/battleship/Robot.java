package edu.duke.qw98.battleship;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class Robot extends TextPlayer{
    public Robot(String name, Board<Character> theBoard, BufferedReader input, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        super(name, theBoard, input, out, shipFactory);
    }

    @Override
    public void moveOneShipToNewSquare(){
        Ship<Character> oldOne = null;
        while(true){
            ArrayList<Ship<Character>> robotShips = theBoard.myShips;
            if(oldOne == null){
                for(Ship<Character> ship : robotShips){
                    if(ship.isSunk())
                        continue;
                    oldOne = ship;
                    break;
                }

            }
            theBoard.myShips.remove(oldOne);
             char[] chars1 = {'V','H'};
             char[] chars2 = {'U','D','L','R'};
             Placement target;
            if(oldOne.getName().equals("Battleship")||oldOne.getName().equals("Carrier")){
                String coordinate = randomCoordinateOfBoard();
                int choice = (int) ((Math.random() * 4) % 4);
                char ch =  chars2[choice];
                StringBuilder place  = new StringBuilder();
                place.append(coordinate).append(ch);
                target = new Placement(place.toString());
            }
            else{
                String coordinate = randomCoordinateOfBoard();
                int choice = (int) ((Math.random() * 2) % 2);
                char ch =  chars1[choice];
                StringBuilder place  = new StringBuilder();
                place.append(coordinate).append(ch);
                target = new Placement(place.toString());
            }
                V2ShipFactory v2Factory = (V2ShipFactory)shipFactory;
                Ship<Character> newShip = v2Factory.makeNewShip(target, oldOne);
                String info = theBoard.tryAddShip(newShip);
                if(info == null)
                    break;
                else{
                    throw new IllegalArgumentException("Wrong Argu in add ships");
                }
        }
    }

    @Override
    public void sonarScan(Board<Character> board){
        Coordinate scanPoint = new Coordinate(randomCoordinateOfBoard());
        String shipInfoAround = board.ScanAndFind(scanPoint);
    }

    public void robotAttack(Board<Character> enemyBoard, BoardTextView enemyView){
        Coordinate point = new Coordinate(randomCoordinateOfBoard());
        System.out.println("The robot fire at the " + point);
        enemyBoard.fireAt(point);
    }

    /**
     * randomly generate the coordinate on the board
     */

    public static String randomCoordinateOfBoard(){
        //a0;
        char x = (char)(Math.random() * 20 + 'a');
        int y = (int)((Math.random()) * 10);
        StringBuilder res = new StringBuilder();
        res.append(x).append(y);
        return res.toString();
    }

}
