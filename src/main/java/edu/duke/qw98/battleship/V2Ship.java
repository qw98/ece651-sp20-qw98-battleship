package edu.duke.qw98.battleship;
import java.util.HashMap;

public class V2Ship<T> extends BasicShip<T> {

    protected final String name;
    public  HashMap<Integer, Coordinate> piecesToMove;


    public void makeCoords(){
    }

    public V2Ship(String name, char orientation, Coordinate refPoint,
                        ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(myDisplayInfo, enemyDisplayInfo, refPoint, orientation);
        this.name             = name;
        this.piecesToMove = new HashMap<>();
        makeCoords();
    }

    @Override
    public String getName() {
        return name;
    }

}
