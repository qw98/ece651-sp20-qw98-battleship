package edu.duke.qw98.battleship;

/**
 * This is the function to check the rule that the ships do not collide with each other
 */
    public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
        public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
            super(next);
        }
        @Override
        protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
            for(Coordinate c1:theShip.getCoordinates()){
                if(theBoard.whatIsAtForSelf(c1)!=null){
                    return "That placement is invalid: the ship overlaps another ship.";
                }
            }
            return null;
        }
    }

